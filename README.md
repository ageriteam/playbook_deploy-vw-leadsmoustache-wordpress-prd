**INFO: Aplicação em Wordpress**

* *URL Produção:* <https://vw-leadsmoustache.ageriservicos.com.br> | <https://...>
* *URL Homologação:* <http://vw-leadsmoustache-hml.ageriservicos.com.br>
* *Diretório App:* /var/deploy/vw-leadsmoustache-wordpress
* *Repositório:* git@bitbucket.org/stefannoo/leads.git

*Observação:* Na task 'GIT | Baixando Branch...', o parâmetro `become_user` foi alterado para `root`.